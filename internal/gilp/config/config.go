// Package config contains configuration data.
package config

import (
	"strings"
	"fmt"
	"github.com/kelseyhightower/envconfig"
	toml "github.com/pelletier/go-toml"
        "net/url"
)

type tGeneralConfig struct {
	Port         int
	Token        string
	GitLabURL    string
}

type tDebugConfig struct {
	DumpRequests bool
}


var General = tGeneralConfig {
	Port:        8080,
	Token:       "",
	GitLabURL:   "https://gitlab.com",
}
var Debug = tDebugConfig {
	DumpRequests: false,
}
var GitLabURL *url.URL

func Load() error {
	// Read config from file
	configToml, err := toml.LoadFile("config.toml")
	if err != nil {
		if strings.HasSuffix(err.Error(), "no such file or directory") {
			//log.Info().Msg("Missing config.toml, using defaults")
		} else {
			return fmt.Errorf("Could not parse config.toml: %w", err)
		}
	} else {
		configToml.Get("General").(*toml.Tree).Unmarshal(&General)
		configToml.Get("Debug").(*toml.Tree).Unmarshal(&Debug)
	}

	// Read config / overwrites from env
	err = envconfig.Process("gilp", &General)
	if err != nil {
		return fmt.Errorf("Could not read env config: %w", err)
	}
	err = envconfig.Process("gilp", &Debug)
	if err != nil {
		return fmt.Errorf("Could not read env config: %w", err)
	}

	// Check required
	if General.Token == "" {
		return fmt.Errorf("Empty Token (required).")
	}

	// Post process options
	if General.GitLabURL == "" {
		return fmt.Errorf("Empty GitLab URL (required).")
	}
	GitLabURL, err = url.Parse(General.GitLabURL)
	if err != nil {
		return fmt.Errorf("Could not parse GitLab URL (required): %w", err)
	}

	return nil
}
