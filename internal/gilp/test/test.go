// Package test contains test data and helpers.
package test

import (
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"

	"gopkg.in/square/go-jose.v2"
)

// Test token from https://jwt.io. Use the link below along with the private key.
// https://jwt.io/#debugger-io?token=eyJraWQiOiJ0ZXN0IiwiYWxnIjoiUlMyNTYiLCJ0eXAiOiJKV1QifQ.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.YmjIxxd1xbnznCOs8-Kc_58gAHZmsRIMqQZhlZ5tXlwFuE6ror593-6QdwL5Ph-QUNMZXkJ8jxR_NevVnF41sthW8O0lnI6f9KlmwKUKQL05QvQcsEUpVIUnjpegDcUhO1wsCKxJf4rufZCGy3-6KLKnAXRmhmYF_kDZ0MFOuOSh9JWLXtRxtlniaAanMiReseIjI18laTNMxv2QmyHEPCWnOJPcmxKlebR8eZTBlsbsRfuKwUCRsCjzFaHHbvU1GSBqsTs6t0recMXio7xxCWMip5syWINo6aFSVPAV6cR4jS49DVFQAoTwFRgTtC3M80mXCWkBAo47sFK0mMne1A&publicKey=-----BEGIN%20PUBLIC%20KEY-----%0AMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnzyis1ZjfNB0bBgKFMSv%0AvkTtwlvBsaJq7S5wA%2BkzeVOVpVWwkWdVha4s38XM%2Fpa%2Fyr47av7%2Bz3VTmvDRyAHc%0AaT92whREFpLv9cj5lTeJSibyr%2FMrm%2FYtjCZVWgaOYIhwrXwKLqPr%2F11inWsAkfIy%0AtvHWTxZYEcXLgAXFuUuaS3uF9gEiNQwzGTU1v0FqkqTBr4B8nW3HCN47XUu0t8Y0%0Ae%2Blf4s4OxQawWD79J9%2F5d3Ry0vbV3Am1FtGJiJvOwRsIfVChDpYStTcHTCMqtvWb%0AV6L11BWkpzGXSW4Hv43qa%2BGSYOD2QU68Mb59oSk2OB%2BBtOLpJofmbGEGgvmwyCI9%0AMwIDAQAB%0A-----END%20PUBLIC%20KEY-----

const (
	testPrivateKeyPEM = `-----BEGIN RSA PRIVATE KEY-----
MIIEogIBAAKCAQEAnzyis1ZjfNB0bBgKFMSvvkTtwlvBsaJq7S5wA+kzeVOVpVWw
kWdVha4s38XM/pa/yr47av7+z3VTmvDRyAHcaT92whREFpLv9cj5lTeJSibyr/Mr
m/YtjCZVWgaOYIhwrXwKLqPr/11inWsAkfIytvHWTxZYEcXLgAXFuUuaS3uF9gEi
NQwzGTU1v0FqkqTBr4B8nW3HCN47XUu0t8Y0e+lf4s4OxQawWD79J9/5d3Ry0vbV
3Am1FtGJiJvOwRsIfVChDpYStTcHTCMqtvWbV6L11BWkpzGXSW4Hv43qa+GSYOD2
QU68Mb59oSk2OB+BtOLpJofmbGEGgvmwyCI9MwIDAQABAoIBACiARq2wkltjtcjs
kFvZ7w1JAORHbEufEO1Eu27zOIlqbgyAcAl7q+/1bip4Z/x1IVES84/yTaM8p0go
amMhvgry/mS8vNi1BN2SAZEnb/7xSxbflb70bX9RHLJqKnp5GZe2jexw+wyXlwaM
+bclUCrh9e1ltH7IvUrRrQnFJfh+is1fRon9Co9Li0GwoN0x0byrrngU8Ak3Y6D9
D8GjQA4Elm94ST3izJv8iCOLSDBmzsPsXfcCUZfmTfZ5DbUDMbMxRnSo3nQeoKGC
0Lj9FkWcfmLcpGlSXTO+Ww1L7EGq+PT3NtRae1FZPwjddQ1/4V905kyQFLamAA5Y
lSpE2wkCgYEAy1OPLQcZt4NQnQzPz2SBJqQN2P5u3vXl+zNVKP8w4eBv0vWuJJF+
hkGNnSxXQrTkvDOIUddSKOzHHgSg4nY6K02ecyT0PPm/UZvtRpWrnBjcEVtHEJNp
bU9pLD5iZ0J9sbzPU/LxPmuAP2Bs8JmTn6aFRspFrP7W0s1Nmk2jsm0CgYEAyH0X
+jpoqxj4efZfkUrg5GbSEhf+dZglf0tTOA5bVg8IYwtmNk/pniLG/zI7c+GlTc9B
BwfMr59EzBq/eFMI7+LgXaVUsM/sS4Ry+yeK6SJx/otIMWtDfqxsLD8CPMCRvecC
2Pip4uSgrl0MOebl9XKp57GoaUWRWRHqwV4Y6h8CgYAZhI4mh4qZtnhKjY4TKDjx
QYufXSdLAi9v3FxmvchDwOgn4L+PRVdMwDNms2bsL0m5uPn104EzM6w1vzz1zwKz
5pTpPI0OjgWN13Tq8+PKvm/4Ga2MjgOgPWQkslulO/oMcXbPwWC3hcRdr9tcQtn9
Imf9n2spL/6EDFId+Hp/7QKBgAqlWdiXsWckdE1Fn91/NGHsc8syKvjjk1onDcw0
NvVi5vcba9oGdElJX3e9mxqUKMrw7msJJv1MX8LWyMQC5L6YNYHDfbPF1q5L4i8j
8mRex97UVokJQRRA452V2vCO6S5ETgpnad36de3MUxHgCOX3qL382Qx9/THVmbma
3YfRAoGAUxL/Eu5yvMK8SAt/dJK6FedngcM3JEFNplmtLYVLWhkIlNRGDwkg3I5K
y18Ae9n7dHVueyslrb6weq7dTkYDi3iOYRW8HRkIQh06wEdbxt0shTzAJvvCQfrB
jg/3747WSsf/zBTcHihTRBdAv6OmdhV4/dD5YBfLAkLrd+mX7iE=
-----END RSA PRIVATE KEY-----`

	// {
	//  "exp": 2606942597,
	//  "sub": "job_123456",
	//  "namespace_id": "123456",
	//  "namespace_path": "foo/bar"
	// }
	JWT = "eyJraWQiOiJ0ZXN0IiwiYWxnIjoiUlMyNTYiLCJ0eXAiOiJKV1QifQ.eyJleHAiOjI2MDY5NDI1OTcsInN1YiI6ImpvYl8xMjM0NTYiLCJuYW1lc3BhY2VfaWQiOiIxMjM0NTYiLCJuYW1lc3BhY2VfcGF0aCI6ImZvby9iYXIifQ.GdSWc4bhVhKnEQMr1tTiKzvW4T0KO6xR1ys3TDu0nPzRn9fYPYVWd0cJ1FlIqWJhUj1dLIAOOPzbHTMVBGYZa0niO4k_3jtdAGcbTIdvPBp48GojvhvYLdiNCNH8evCGK4hUCRgvdDdrnz6uzCVNxujqBR2GEcd2rvG12_hUK7tgWpWXt928D7wbE7cC0NDgwZ4L39bQD9eeOK-jp8-CdW6GaG4KPhV1qRleZ4UahY0wP8hNFIhJmL2xHLwyOkfPD12QCTW0IwL6dVcpZ9_CpwUYOTvCSpd8ejloPR3pN2NEvtJklrWs25X2v3NbOsSXY_GAiwhgkhR4a3haqZmorw"

	// {
	//  "namespace_id": "1",
	//  "namespace_path": "mygroup",
	//  "project_id": "22",
	//  "project_path": "mygroup/myproject",
	//  "user_id": "42",
	//  "user_login": "myuser",
	//  "user_email": "myuser@example.com",
	//  "pipeline_id": "1212",
	//  "job_id": "1212",
	//  "ref": "master",
	//  "ref_type": "branch",
	//  "ref_protected": "true",
	//  "jti": "c82eeb0c-5c6f-4a33-abf5-4c474b92b558",
	//  "iss": "gitlab.com",
	//  "iat": 1606938997,
	//  "nbf": 1606938992,
	//  "exp": 1606942597,
	//  "sub": "job_1212"
	// }
	GitLabJWT = "eyJraWQiOiJ0ZXN0IiwiYWxnIjoiUlMyNTYiLCJ0eXAiOiJKV1QifQ.eyJuYW1lc3BhY2VfaWQiOiIxIiwibmFtZXNwYWNlX3BhdGgiOiJteWdyb3VwIiwicHJvamVjdF9pZCI6IjIyIiwicHJvamVjdF9wYXRoIjoibXlncm91cC9teXByb2plY3QiLCJ1c2VyX2lkIjoiNDIiLCJ1c2VyX2xvZ2luIjoibXl1c2VyIiwidXNlcl9lbWFpbCI6Im15dXNlckBleGFtcGxlLmNvbSIsInBpcGVsaW5lX2lkIjoiMTIxMiIsImpvYl9pZCI6IjEyMTIiLCJyZWYiOiJtYXN0ZXIiLCJyZWZfdHlwZSI6ImJyYW5jaCIsInJlZl9wcm90ZWN0ZWQiOiJ0cnVlIiwianRpIjoiYzgyZWViMGMtNWM2Zi00YTMzLWFiZjUtNGM0NzRiOTJiNTU4IiwiaXNzIjoiZ2l0bGFiLmNvbSIsImlhdCI6MTYwNjkzODk5NywibmJmIjoxNjA2OTM4OTkyLCJleHAiOjE2MDY5NDI1OTcsInN1YiI6ImpvYl8xMjEyIn0.h7CXr-bYMsT0HeGU1aDpH07BDfZGOh717LtFnvZdyKDjKSQimiQrXmNXmhcFjT4hwxbOOAxSxWo3kpHMfmGp75sUHPtYfmnHsBhqIDEAnNdnGH8mgFwlwkH_G4Lhkd_rWpwS3eMCZK1Gj0HOW4cu70U9R2CBAXEwf_vH0mvBBH5xdrG4bGtWMWXV2Vxnl185soRfIgBcaaIPEhLZYIEVVDolMPXDRndhbwilmHkCN8AxENb2DXzPEN_JU6XfChAkhaiuqbwdVLodN8bo7hwm-1v6INc6w_hy982JoIvlesJLev-0YG5-cJVZrxNv50M-gkoECy1yTyf9f0L0zAm8IQ"
)

var (
	testRSAPrivateKey = mustUnmarshalRSAPrivateKey(testPrivateKeyPEM)

	JWKS = &jose.JSONWebKeySet{
		Keys: []jose.JSONWebKey{
			{
				KeyID: "test",
				Key:   &testRSAPrivateKey.PublicKey,
			},
		},
	}
)

func mustUnmarshalRSAPrivateKey(data string) *rsa.PrivateKey {
	block, _ := pem.Decode([]byte(data))
	if block == nil {
		panic("failed to decode PEM data")
	}

	key, err := x509.ParsePKCS1PrivateKey(block.Bytes)
	if err != nil {
		panic("failed to parse RSA key: " + err.Error())
	}

	return key
}
