package jwt

import (
	"testing"

	"gitlab.com/johanngyger/gilp/internal/gilp/test"
)

func TestClaimsInvalidJWT(t *testing.T) {
	_, _, err := ExtractClaims("", test.JWKS)
	if err == nil {
		t.Errorf("Expecting an error because of invalid JWT")
	}
}

func TestClaimsValidJWTInvalidJWKS(t *testing.T) {
	_, _, err := ExtractClaims(test.JWT, &GitLabJWKS)
	if err == nil {
		t.Errorf("Expecting an error because of invalid JWT")
	}
}

func TestClaimsValidJWT(t *testing.T) {
	c, glc, err := ExtractClaims(test.JWT, test.JWKS)
	if err != nil {
		t.Error("No error expect here, the JWT should be valid:", err)
	}

	values := map[interface{}]interface{}{
		"job_123456": c.Subject,
		"123456":     glc.NamespaceID,
		"foo/bar":    glc.NamespacePath,
	}

	for expected, actual := range values {
		if expected != actual {
			t.Errorf("Invalid data in JWT payload: expected=%v, actual=%v", expected, actual)
		}
	}
}

func TestClaimsGitLabJWT(t *testing.T) {
	c, glc, err := ExtractClaims(test.GitLabJWT, test.JWKS)
	if err != nil {
		t.Error("No error expect here, the JWT should be valid:", err)
	}

	values := map[interface{}]interface{}{
		"gitlab.com":                           c.Issuer,
		"job_1212":                             c.Subject,
		"c82eeb0c-5c6f-4a33-abf5-4c474b92b558": c.ID,
		"1":                                    glc.NamespaceID,
		"mygroup":                              glc.NamespacePath,
	}

	for expected, actual := range values {
		if expected != actual {
			t.Errorf("Invalid data in JWT payload: expected=%v, actual=%v", expected, actual)
		}
	}
}
