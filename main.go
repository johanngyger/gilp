package main

import (
	"gitlab.com/johanngyger/gilp/internal/gilp"
)

func main() {
	gilp.Start()
}
