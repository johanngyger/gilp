module gitlab.com/johanngyger/gilp

go 1.15

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/google/go-cmp v0.5.4 // indirect
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/kr/pretty v0.1.0 // indirect
	github.com/pelletier/go-toml v1.8.1 // indirect
	github.com/rs/zerolog v1.20.0
	github.com/stretchr/testify v1.4.0 // indirect
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/square/go-jose.v2 v2.5.1
)

replace (
	gitlab.com/johanngyger/gilp/internal/gilp => ./internal/gilp
	gitlab.com/johanngyger/gilp/internal/gilp/auth => ./internal/gilp/auth
	gitlab.com/johanngyger/gilp/internal/gilp/config => ./internal/gilp/config
	gitlab.com/johanngyger/gilp/internal/gilp/jwt => ./internal/gilp/jwt
)
